refresh()
window.onresize = function () {  //监听屏幕的改变
    setTimeout(function () {
        refresh();
    }, 10)
};

function refresh() {
    let devicewidth = document.documentElement.clientWidth;


    let list1 = document.querySelector(".list1");
    let head2 = document.querySelector(".head-2");
    let list2 = document.querySelector(".head-2 .list2");
    let list2btn = document.querySelector(".list2btn");
    let banner = document.querySelector(".banner");
    if (devicewidth < 770) {
        list1.style.display = "none";
        head2.style.height = "220px";
        list2.style.display = "flex";
        list2btn.style.display = "block";
        banner.style = "margin-top:100px;"
    } else {
        list1.style.display = "flex";
        list2btn.style.display = "none";
        list2.style.display = "none";
        banner.style = "margin-top:20px;"
    }
}


// jquery
$(function () {
    // list2
    let list2 = $(".head-2  .list2");
    let lista = $(".list2  .list2-2");

    for (let i = 0; i < list2.children().children().length; i++) {
        let j = list2.children().children().eq(i);
        j.children().eq(0).on("click", function () {
            $(this).fadeOut(2000).siblings().fadeIn(2000);
        });

        list2.children().children().eq(i).children().eq(1).on("click", function () {
            setTimeout(function () {

                list2.children().children().eq(i).children().eq(1).fadeOut(2000).siblings().fadeIn(2000);
            }, 4000)
        });

    }

    let btnnum = 0;
    let flag = true;
    // lunbo
    $(".list2btn").children().eq(1).on("click", function () {
        if (flag == true) {

            btnnum++
            $(".list-u").animate({ left: btnnum * (-100) + "%" })
            if (btnnum == $(".list-u").children().length) {
                btnnum = 0;
                $(".list-u").animate({ left: btnnum }, 1)
            }
            flag = false;
        }
        setTimeout(function () {
            flag = true;
        }, 1500)
    })
    $(".list2btn").children().eq(0).on("click", function () {
        if (flag == true) {
            btnnum--
            if (btnnum < 0) {
                btnnum = 2;

                $(".list-u").animate({ left: "-200%" });

            }
            $(".list-u").animate({ left: btnnum * (-100) + "%" })
            flag = false;
        }
        setTimeout(function () {
            flag = true;
        }, 1500)
    })
})